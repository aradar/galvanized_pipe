from .galvanized_pipe import *


__doc__ = galvanized_pipe.__doc__
if hasattr(galvanized_pipe, "__all__"):
    __all__ = galvanized_pipe.__all__
