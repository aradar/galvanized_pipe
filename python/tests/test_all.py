import time

import numpy as np
import pytest
import galvanized_pipe


def test_all_rust_funcs():
    galvanized_pipe.load_image("BlackMarble_2016_928m_europe.png")
    result = galvanized_pipe.load_image_and_return_pointer("BlackMarble_2016_928m_europe.png")
    galvanized_pipe.do_something_with_pointer(result)
