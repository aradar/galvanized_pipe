use std::path::{Path, PathBuf};
use image::DynamicImage;
use image::io::Reader as ImageReader;
use numpy::ndarray::{Array, ArrayBase, Dim, Ix, OwnedRepr};
use numpy::{
    IntoPyArray, PyArray3
};
use pyo3::prelude::*;


fn load_image(path: &Path) -> ArrayBase<OwnedRepr<u8>, Dim<[Ix; 3]>> {
    let img = ImageReader::open(path)
        .unwrap()
        .decode()
        .unwrap();

    let width = img.width() as usize;
    let height = img.height() as usize;
    let channels = 3usize;
    let pixel_values = img.into_bytes();

    let pixel_array = Array::from_vec(pixel_values)
        .into_shape((channels, height, width))
        .unwrap();

    pixel_array
}

#[pyfunction]
#[pyo3(name = "load_image")]
fn load_image_py(py: Python<'_>, path: PathBuf) -> Bound<'_, PyArray3<u8>> {
    let pixel_array = py.allow_threads(|| load_image(&path));
    pixel_array.into_pyarray_bound(py)
}

#[pyclass]
struct ClassWithRustPointer {
    image: DynamicImage,
}

#[pyfunction]
#[pyo3(name = "load_image_and_return_pointer")]
fn load_image_and_return_pointer_py(path: PathBuf) -> ClassWithRustPointer {
    let image = ImageReader::open(path)
        .unwrap()
        .decode()
        .unwrap();

    ClassWithRustPointer{ image }
}

#[pyfunction]
#[pyo3(name = "do_something_with_pointer")]
fn do_something_with_pointer_py(pointer: &ClassWithRustPointer) {
    println!("{}", pointer.image.height());
}

#[pymodule]
fn galvanized_pipe(m: &Bound<'_, PyModule>) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(load_image_py, m)?)?;
    m.add_function(wrap_pyfunction!(load_image_and_return_pointer_py, m)?)?;
    m.add_function(wrap_pyfunction!(do_something_with_pointer_py, m)?)?;

    Ok(())
}
